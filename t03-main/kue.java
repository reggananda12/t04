public abstract class kue{

    private String name;
    private double price;
    private double berat;
    
    public kue(String name, double price, double berat){
        this.name = name;
        this.price = price;
        this.berat = berat;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public double getPrice() {
        return price;
    }
    public void setPrice(double price) {
        this.price = price;
    }
    public double getBerat(){
        return this.berat;
    }

    abstract public double hitungHarga();
    abstract public double Berat();
    abstract public double Jumlah();
    @Override
    public String toString(){
        return String.format("\n---------- "+getName()+" ----------"+"\nHarga\t\t:"+getPrice());
    }
}