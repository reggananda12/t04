class readystock extends kue{

    private double jumlah;

    public readystock(String name, double price, int jumlah) {
        super (name, price, jumlah);
        this.jumlah = jumlah;

    }
    public double hitungHarga() {
        return getPrice() * jumlah * 2;
    }
    public double Berat() {
        return 0;
    }
    public double Jumlah() {
        return hitungHarga();
    }
    public void setJumlah(double jumlah) {
        this.jumlah = jumlah;
    }
    public String toString() {
        return super.toString() + String.format("\nJumlah\t\t: %.2f\nHarga total\t: %.2f", jumlah, Jumlah());
    }
}